package ru.sam.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintWriter;

/**
 * @author gashchenko.semen
 * @since 27.05.2016
 */
public class TestIoService extends IoServiceImpl {
	private static final Logger log = LoggerFactory.getLogger(TestIoService.class);

	int lower, upper;
	final PrintWriter writer;

	TestIoService(InputStream in, OutputStream out, BundleService bundleService, Level level, String tempFilePath) throws IOException {
		super(in, out, bundleService);
		lower = 0;
		upper = level.upper();

		writer = new PrintWriter(new PipedOutputStream((PipedInputStream) in), true);
		writer.println("sam");
		writer.println(tempFilePath);
		writer.println(level.ordinal() + 1);
	}

	@Override
	public int getGuess(int guessNum, Level level) {
		log.debug("Lower: {}, upper: {}", lower, upper);
		return getMiddle();
	}

	@Override
	public void printSmallerMsg() {
		upper = getMiddle();
	}

	@Override
	public void printBiggerMsg() {
		lower = getMiddle() + 1;
	}

	@Override
	public void printGotItMsg() {
		writer.println("q");
		writer.println("y");
		log.debug("QUIT");
	}

	int getMiddle() {
		return (lower + upper) >>> 1;
	}
}
