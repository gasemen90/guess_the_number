package ru.sam.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.asList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;


public class AppTest {
    private static final Logger log = LoggerFactory.getLogger(AppTest.class);

    @Test(dataProvider = "calcData")
    public void testCalcPoints(Level level, Integer guessCount, Integer points) {
        assertEquals(App.calcPoints(level, guessCount), (int) points);
    }

    @DataProvider(name = "calcData")
    public Object[][] calcData() {
        return new Object[][] {
                { Level.EASY,   1,  10 },
                { Level.EASY,   10, 1  },
                { Level.MEDIUM, 1,  10 },
                { Level.MEDIUM, 10, 3  },
                { Level.HARD,   1,  10 },
                { Level.HARD,   10, 5  }
        };
    }

    @Test
    public void testStat() throws IOException {
        final IoService io = mock(IoService.class);
        when(io.getFileName()).thenReturn(getTempPath());

        final StorageService storage = new StorageService(io);
        storage.init();
        final Map<String, Double> expected = new HashMap<>();
        for (String username : asList("user1", "user2", "user3")) {
            expected.put(username, 16.5);
            for (Level level : Level.values()) {
                for (int point = 1; point <= 10; point++) {
                    storage.write(username, level.ordinal() + 1, point);
                }
            }
        }
        assertEquals(storage.getStat(), expected);
    }

    @Test
    public void gameTest() throws IOException {
        final StorageService storage = mock(StorageService.class);
        final BundleService bundle = mock(BundleService.class);
        when(bundle.getMessage(anyString())).thenReturn("");

        final OutputStream gameOs = new OutputStream() {
            @Override
            public void write(int b) throws IOException {
                // empty
            }
        };

        final String tempFilePath = getTempPath();
        for (Level level : Level.values()) {
            final InputStream is = new PipedInputStream();
            final IoService io = new TestIoService(is, gameOs, bundle, level, tempFilePath);
            new App(storage, io).run();
            is.close();
        }
    }

    public static String getTempPath() {
        try {
            final File temp = File.createTempFile("guessTest", "temp");
            temp.createNewFile();
            temp.deleteOnExit();
            return temp.getAbsolutePath();
        } catch (IOException ioe) {
            log.debug(ioe.getMessage(), ioe);
            throw new RuntimeException(ioe);
        }
    }

}
