package ru.sam.game;

import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class BundleService {
    private static final Map<String, String> CACHE = new HashMap<>();
    private final ResourceBundle bundle;

    public BundleService(ResourceBundle resourceBundle) {
        bundle = resourceBundle;
    }

    public String getMessage(String label) {
        if (!CACHE.containsKey(label)) {
            CACHE.put(label, bundle.getString(label));
        }
        return CACHE.get(label);
    }
}
