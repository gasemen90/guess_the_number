package ru.sam.game;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CancellationException;


public class App implements Runnable {
    private static final Logger log = LoggerFactory.getLogger(App.class);

    private final StorageService storage;
    private final IoService io;

    @Inject
    public App(StorageService storageService, IoService ioService) {
        storage = storageService;
        io = ioService;
    }

    @Override
    public void run() {
        try {
            io.printSplash();
            final String username = io.getUsername();
            storage.init();
            int gameCounter = 0;

            while (true) {
                io.printLevels();
                final Level level = io.getLevel();
                final int num = new Random(System.currentTimeMillis()).nextInt(level.upper() - 1) + 1;
                int guessCounter = 0;
                io.printGameNum(++gameCounter);
                io.printGuessRange(level);
                int guess = -1;
                while (num != guess) {
                    guess = io.getGuess(++guessCounter, level);
                    log.debug("Level: {}, num: {}, guess: {}", level.name(), num, guess);

                    if (num < guess) {
                        io.printSmallerMsg();
                    } else if (num > guess) {
                        io.printBiggerMsg();
                    } else {
                        io.printGotItMsg();
                    }
                }
                storage.write(username, level.ordinal() + 1, calcPoints(level, guessCounter));
            }
        } catch (CancellationException ce) {
            final Map<String, Double> stat = storage.getStat();
            io.printRank(stat);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            io.printErrorOccurred(e.getLocalizedMessage());
        }
    }

    static int calcPoints(Level level, int guessNum) {
        final int nDigit = String.valueOf(level.upper()).length();
        return (int) Math.round(Math.pow((1.0 / guessNum) * (level.upper() + 1), 1.0 / nDigit));
    }

}
