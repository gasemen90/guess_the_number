package ru.sam.game;

import java.util.Map;

interface IoService {
    String SPLASH = "\n" +
            " __         ___    __   __     _|_  |__   ___       __         __ __  |__   ___   __  \n" +
            "(__| (__(_ (__/_ __)  __)       |_, |  ) (__/_     |  ) (__(_ |  )  ) |__) (__/_ |  ' \n" +
            " __/                                                                                  \n";

    String PROMPT_CHOOSEL_EVEL = "prompt.chooseLevel";
    String ACTION_ARE_YOU_SURE_YOU_WANT_TO_EXIT = "action.areYouSureYouWantToExit";
    String WARN_VALUE_MUST_BE_INTEGER_IN_RANGE = "warn.valueMustBeIntegerInRange";
    String WARN_VALUE_MUST_BE_IN_RANGE = "warn.valueMustBeInRange";
    String PROMPT_ENTER_YOUR_NAME = "prompt.enterYourName";
    String PROMPT_ENTER_THE_PATH_TO_DB_FILE = "prompt.enterThePathToDbFile";
    String GAME_TITLE = "game.title";
    String INFO_NEW_FILE_IS_CREATED = "info.newFileIsCreated";
    String INFO_GAME_NUM = "info.gameNum";
    String INFO_GUESS_NUM = "info.guessNum";
    String INFO_LEVELS = "info.levels";
    String GAME_NUMBER_RANGE = "game.numberRange";
    String GAME_SMALLER_MSG = "game.smallerMsg";
    String GAME_BIGGER_MSG = "game.biggerMsg";
    String GAME_GOT_IT = "game.gotIt";
    String RANK_HEADER = "rank.header";
    String ERROR_MAIN = "error.main";

    int readInt(String prompt, int min, int max);

    String getUsername();

    Level getLevel();

    String getFileName();

    int getGuess(int guessNum, Level level);

    void printLevels();

    void printSplash();

    void printFileInitMsg(String path);

    void printGameNum(int gameNum);

    void printGuessRange(Level level);

    void printSmallerMsg();

    void printBiggerMsg();

    void printGotItMsg();

    void printRank(Map<String, Double> data);

    void printErrorOccurred(String msg);
}
