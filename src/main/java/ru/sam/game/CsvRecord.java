package ru.sam.game;


public class CsvRecord {
    protected static final String[] COLUMNS = new String[] {"username", "level", "points"};

    private String username;

    private Integer level;

    private Integer points;

    public CsvRecord() {
        // empty
    }

    public CsvRecord(String username, Integer level, Integer points) {
        this.username = username;
        this.level = level;
        this.points = points;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
}
