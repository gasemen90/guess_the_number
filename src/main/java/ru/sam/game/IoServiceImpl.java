package ru.sam.game;

import com.google.inject.Inject;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.CancellationException;

public class IoServiceImpl implements IoService {
    private final PrintWriter out;
    private final Scanner in;
    private final BundleService bundle;

    @Inject
    public IoServiceImpl(InputStream in, OutputStream out, BundleService bundleService) {
        this.in = new Scanner(in);
        this.out = new PrintWriter(out, true);
        this.bundle = bundleService;
    }

    private static boolean isQuit(String token) {
        return token.equalsIgnoreCase("q") || token.equalsIgnoreCase("quit");
    }

    private static boolean isYes(String token) {
        return token.equalsIgnoreCase("y");
    }

    private static boolean isInteger(String token) {
        return token.matches("-?\\d+");
    }

    private String readStr(String prompt) {
        out.printf("%s: ", prompt);
        String result = in.next();
        while (isQuit(result)) {
            out.printf("%s: ", bundle.getMessage(ACTION_ARE_YOU_SURE_YOU_WANT_TO_EXIT));
            final String answer = in.next();
            if (isYes(answer)) {
                throw new CancellationException();
            } else {
                out.printf("%s: ", prompt);
                result = in.next();
            }
        }
        return result;
    }

    @Override
    public int readInt(String prompt, int min, int max) {
        int num;
        while (true) {
            final String result = readStr(prompt);
            if (!isInteger(result)) {
                out.printf(bundle.getMessage(WARN_VALUE_MUST_BE_INTEGER_IN_RANGE), min, max);
                out.println();
                continue;
            }
            num = Integer.valueOf(result);
            if (num < min || num > max) {
                out.printf(bundle.getMessage(WARN_VALUE_MUST_BE_IN_RANGE), min, max);
                out.println();
                continue;
            }
            break;
        }
        return num;
    }

    @Override
    public String getUsername() {
        return readStr(bundle.getMessage(PROMPT_ENTER_YOUR_NAME));
    }

    @Override
    public Level getLevel() {
        final String prompt = bundle.getMessage(PROMPT_CHOOSEL_EVEL);
        final int num = readInt(prompt, 1, Level.values().length);
        return Level.values()[num - 1];
    }

    @Override
    public String getFileName() {
        return readStr(bundle.getMessage(PROMPT_ENTER_THE_PATH_TO_DB_FILE));
    }

    @Override
    public int getGuess(int guessNum, Level level) {
        return readInt(String.format(bundle.getMessage(INFO_GUESS_NUM), guessNum), 0, level.upper());
    }

    @Override
    public void printLevels() {
        for (Level level : Level.values()) {
            out.printf(bundle.getMessage(INFO_LEVELS), level.ordinal() + 1, level.upper());
            out.println();
        }
    }

    @Override
    public void printSplash() {
        out.println(SPLASH);
        out.println(bundle.getMessage(GAME_TITLE));
        out.println();
    }

    @Override
    public void printFileInitMsg(String path) {
        out.printf(bundle.getMessage(INFO_NEW_FILE_IS_CREATED), path);
        out.println();
    }

    @Override
    public void printGameNum(int gameNum) {
        out.printf(bundle.getMessage(INFO_GAME_NUM), gameNum);
        out.println();
    }

    @Override
    public void printGuessRange(Level level) {
        out.printf(bundle.getMessage(GAME_NUMBER_RANGE), level.upper());
        out.println();
    }

    @Override
    public void printSmallerMsg() {
        out.println(bundle.getMessage(GAME_SMALLER_MSG));
    }

    @Override
    public void printBiggerMsg() {
        out.println(bundle.getMessage(GAME_BIGGER_MSG));
    }

    @Override
    public void printGotItMsg() {
        out.println(bundle.getMessage(GAME_GOT_IT));
        out.println();
    }

    @Override
    public void printRank(Map<String, Double> data) {
        if (!data.isEmpty()) {
            out.println();
            out.println(bundle.getMessage(RANK_HEADER));
            int i = 0;
            for (Map.Entry<String, Double> entry : data.entrySet()) {
                out.printf("%d. %s\t\t%.0f\n", ++i, entry.getKey(), entry.getValue());
            }
        }
    }

    @Override
    public void printErrorOccurred(String msg) {
        out.printf(bundle.getMessage(ERROR_MAIN), msg);
        out.println();
    }

}
