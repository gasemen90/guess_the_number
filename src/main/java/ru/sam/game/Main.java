package ru.sam.game;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;
import java.util.ResourceBundle;

public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        log.debug("App started");

        final Injector injector = Guice.createInjector(new AppModule());

        final App app = injector.getInstance(App.class);
        try {
            new Thread(app).start();
        } catch (Throwable t) {
            log.error(t.getMessage(), t);
        }

    }

    static class AppModule extends AbstractModule {
        @Override
        protected void configure() {
            bind(BundleService.class).toInstance(new BundleService(ResourceBundle.getBundle("messages", Locale.getDefault())));
            bind(IoService.class).to(IoServiceImpl.class);
            bind(InputStream.class).toInstance(System.in);
            bind(OutputStream.class).toInstance(System.out);
        }
    }
}
