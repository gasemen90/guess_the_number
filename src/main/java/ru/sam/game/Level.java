package ru.sam.game;

public enum Level {

    EASY(9), MEDIUM(99), HARD(999);

    private final int upper;

    Level(int upper) {
        this.upper = upper;
    }

    public int upper() {
        return upper;
    }
}
