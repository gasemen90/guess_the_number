package ru.sam.game;

import com.google.inject.Inject;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class StorageService {
    private static final CellProcessor[] CELL_PROCESSORS = getProcessors();

    private final IoService io;
    private File db;
    private List<CsvRecord> local;

    @Inject
    public StorageService(IoService ioService) {
        io = ioService;
    }

    private static CellProcessor[] getProcessors() {
        return new CellProcessor[]{
                new NotNull(),
                new NotNull(new ParseInt()),
                new NotNull(new ParseInt())
        };
    }

    public void init() throws IOException {
        final String path = io.getFileName();
        db = new File(path);
        local = new ArrayList<>();

        if (!db.exists()) {
            if (!db.createNewFile()) {
                throw new RuntimeException("Cannot create file " + path);
            }
            io.printFileInitMsg(db.getAbsolutePath());
        }

        if (db.length() == 0) {
            try (ICsvBeanWriter beanWriter = new CsvBeanWriter(new FileWriter(db), CsvPreference.EXCEL_PREFERENCE)) {
                beanWriter.writeHeader(CsvRecord.COLUMNS);
            }
        } else {
            try (ICsvBeanReader beanReader = new CsvBeanReader(new FileReader(db), CsvPreference.EXCEL_PREFERENCE)) {
                beanReader.getHeader(true);
                CsvRecord record;
                while ((record = beanReader.read(CsvRecord.class, CsvRecord.COLUMNS, CELL_PROCESSORS)) != null) {
                    local.add(record);
                }
            } catch (Exception e) {
                throw new RuntimeException("Invalid format for " + path);
            }
        }
    }

    public void write(String username, Integer level, Integer points) throws IOException {
        assert db != null;
        final CsvRecord record = new CsvRecord(username, level, points);
        try (ICsvBeanWriter beanWriter = new CsvBeanWriter(new FileWriter(db, true), CsvPreference.EXCEL_PREFERENCE)) {
            beanWriter.write(record, CsvRecord.COLUMNS, CELL_PROCESSORS);
        }
        local.add(record);
    }

    public Map<String, Double> getStat() {
        if (local == null || local.isEmpty()) {
            return Collections.emptyMap();
        }

        final Map<String, Double> resultMap = new HashMap<>();

        final Map<Integer, List<CsvRecord>> byLevel = local.stream()
                .collect(Collectors.groupingBy(CsvRecord::getLevel));

        for (Map.Entry<Integer, List<CsvRecord>> byLevelEntry : byLevel.entrySet()) {
            final Map<String, Double> avgByUser = byLevelEntry.getValue().stream()
                    .collect(Collectors.groupingBy(
                            CsvRecord::getUsername,
                            Collectors.averagingInt(CsvRecord::getPoints)));
            for (Map.Entry<String, Double> avgEntry : avgByUser.entrySet()) {
                if (!resultMap.containsKey(avgEntry.getKey())) {
                    resultMap.put(avgEntry.getKey(), .0);
                }
                resultMap.put(avgEntry.getKey(), resultMap.get(avgEntry.getKey()) + avgEntry.getValue());
            }
        }

        return resultMap.entrySet().stream().sorted((c1, c2) -> c2.getValue().compareTo(c1.getValue())).
                collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new));
    }
}
